<?php

class Database{

	public function getConn(){
		$db_host = "localhost";
    	$db_name = "cms";
    	$db_user = "root";
    	$db_pass = "";

    	$dsn = 'mysql:host=' .$db_host. ';db_name=' .$db_name. ';charset=utf-8';

    	return new PDO($dsn, $db_user, $db_pass);
	}

}