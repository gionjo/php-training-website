<?php

require '../includes/init.php';
session_start();
$conn = require '../includes/db.php';



Auth::requireLogin();

$articles = Article::getAll($conn);

?>
<?php require '../includes/header.php'; ?>


<h2>Administration</h2>
<p><a href="new-article.php">New article</a></p>

<?php if (empty($articles)): ?>
    <p>No articles found.</p>
<?php else: ?>

    <table>
        <thead>
            <th>Title</th>
        </thead>
        <tbody>
        <?php foreach ($articles as $article): ?>
            <tr>
                <td>
                   <a href="article.php?id=<?= $article['id']; ?>"><?= htmlspecialchars($article['title']); ?></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    
<?php endif; ?>

<?php require '../includes/footer.php'; ?>
