<!DOCTYPE html>
<html>
<head>
    <title>My blog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/php_train/css/style.css">
</head>
<body>
    <div class="container">
    <header>
        <h1>My blog</h1>
    </header>


    <nav>
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/php_train/">Home</a>
            </li>

            <?php if (Auth::isLoggedIn()) : ?>
                
                <li class="nav-item">
                    <a class="nav-link" href="/php_train/admin/">Admin</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/php_train/logout.php">Log out</a>
                </li>

            <?php else : ?>
                
                <li class="nav-item">
                    <a class="nav-link" href="/php_train/login.php">Log in</a>
                </li>

            <?php endif; ?>
        </ul>
    </nav>

    <main>
